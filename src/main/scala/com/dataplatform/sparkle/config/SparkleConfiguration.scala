package com.dataplatform.sparkle.config

import com.dataplatform.sparkle.spray.text.TextInputConfig

/**
 * @author sarajagopal
 */

trait SparkleConfiguration {
  def sparkJobConf: SparkJobConf
  def sparkContextConfig: SparkContextConfig
  def hadoopConfiguration: Map[String, String]
  def yarnConfiguration: Map[String, String]
}



