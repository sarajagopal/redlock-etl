package com.dataplatform.sparkle.spray.sequence

import com.dataplatform.sparkle.config.SparkleInputConfig

case class SequenceInputConfig(sprayName: String, path: String, partitons: Int)
    extends SparkleInputConfig
