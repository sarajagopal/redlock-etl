package com.dataplatform.sparkle.spray.text

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import com.dataplatform.sparkle.spray.SprayInputProcessor
import com.dataplatform.sparkle.spray.rdd.RDDInSpray

class TextInSpray(context: SparkContext, textInputConfig: TextInputConfig) extends RDDInSpray[String](context, textInputConfig) {

  override def inSpray(): RDD[String] = {
   SprayInputProcessor.readText(context, textInputConfig.path, textInputConfig.partitons)
  }

}