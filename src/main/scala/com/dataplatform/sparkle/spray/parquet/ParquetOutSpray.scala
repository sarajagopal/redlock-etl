package com.dataplatform.sparkle.spray.parquet

import com.dataplatform.sparkle.spray.dataframe.DataFrameOutSpray
import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import org.apache.spark.sql.DataFrame

class ParquetOutSpray(parquetOutputConfig: ParquetOutputConfig) extends DataFrameOutSpray(parquetOutputConfig) {

  def outSpray(df: DataFrame) {
    SparkleOutputProcessor.writeParquetFile(df, parquetOutputConfig.directoryPath, parquetOutputConfig.saveMode, parquetOutputConfig.rePartition)
  }

}