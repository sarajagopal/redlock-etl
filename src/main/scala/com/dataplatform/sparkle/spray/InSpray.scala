package com.dataplatform.sparkle.spray

import com.dataplatform.sparkle.config.SparkleInputConfig

abstract class InSpray[T](sparkleInputConfig: SparkleInputConfig){
  
  def preSpray() 
  
  
  def inSpray():T
  
  def execute():T ={
    preSpray()
    val result = inSpray()
    postSpray()
    result
  } 
  
  def postSpray() 
}