package com.dataplatform.sparkle.spray.text

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
  * @author sarajagopal
  */
case class TextInputConfig(sprayName: String, path: String, partitons: Int)
    extends SparkleInputConfig
