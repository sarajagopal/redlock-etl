package com.dataplatform.sparkle.util

import com.amazonaws.auth.{
  AWSCredentialsProvider,
  DefaultAWSCredentialsProviderChain
}
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{GetObjectRequest, ListObjectsRequest}

object S3Client {
  private val awsCredentialsProvider: AWSCredentialsProvider =
    new DefaultAWSCredentialsProviderChain()
  private val s3Client = new AmazonS3Client(awsCredentialsProvider)

  private def getBucketAndKey(s3URL: String): (String, String) = {
    val regex = "^s3[an]?://([^/]*?)/(.*)$".r

    s3URL match {
      case regex(bucket, key) => (bucket, key)
      case regex(bucket, _)   => (bucket, "")
      case _ =>
        println(s"Could not find bucket and key in $s3URL")
        throw new IllegalArgumentException(s"Invalid S3 URL: $s3URL")
    }
  }

  def ls(s3URL: String) = {
    val (bucket, key) = getBucketAndKey(s3URL)
    import scala.collection.convert.wrapAll._
    s3Client
      .listObjects(
        new ListObjectsRequest().withBucketName(bucket).withPrefix(key))
      .getObjectSummaries
      .map(summary => {
        val bucket = summary.getBucketName
        val key = summary.getKey
        s"s3://$bucket/$key"
      })
  }

  def rm(S3Url: String) = {
    val (bucket, key) = getBucketAndKey(S3Url)
    s3Client.deleteObject(bucket, key)
  }

  def cat(S3Url: String) = {
    val (bucket, key) = getBucketAndKey(S3Url)
    scala.io.Source
      .fromInputStream(
        s3Client.getObject(new GetObjectRequest(bucket, key)).getObjectContent)
      .mkString
  }

  def shutdown = s3Client.shutdown
}
