package com.dataplatform.sparkle.spray.sequence

import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.spray.text.TextFileOutputConfig
import com.dataplatform.sparkle.spray.rdd.RDDOutSpray
import org.apache.hadoop.io.compress.CompressionCodec
import org.apache.hadoop.io.Writable

class SequenceOutSpray(compressionCodec: Class[_ <: CompressionCodec],
                       sequenceOutputConfig: SequenceOutputConfig)
    extends RDDOutSpray[(Writable, Writable)](sequenceOutputConfig) {

  def outSpray(rdd: RDD[(Writable, Writable)]) {
    SparkleOutputProcessor.writeSequence(rdd,
                                         sequenceOutputConfig.path,
                                         sequenceOutputConfig.partitons,
                                         compressionCodec)
  }

}
