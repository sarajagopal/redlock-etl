package com.dataplatform.sparkle.spray.dstream

import com.dataplatform.sparkle.config.SparkleInputConfig
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import com.dataplatform.sparkle.spray.InSpray

abstract case class DStreamInSpray[T](context: StreamingContext, sparkleInputConfig: SparkleInputConfig) extends InSpray[DStream[T]](sparkleInputConfig) with Serializable{ 
  
   def preSpray(){
    
  }
 
  def inSpray():DStream[T] 
  
    
  def postSpray(){
    
  }
}