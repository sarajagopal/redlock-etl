package com.dataplatform.sparkle.spray.kafka

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
  * @author sarajagopal
  */
case class KafkaInputConfig(val sprayName: String,
                            var zkQuorum: String,
                            var group: String,
                            var topics: String,
                            var numThreads: Int)
    extends SparkleInputConfig
