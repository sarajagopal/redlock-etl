package com.dataplatform.sparkle.spray.rdd

import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.config.SparkleOutputConfig
import com.dataplatform.sparkle.spray.OutSpray

abstract class RDDOutSpray[T](sparkleOutputConfig: SparkleOutputConfig) extends OutSpray[RDD[T]](sparkleOutputConfig) with Serializable {

  def preSpray(){
    
  }
  
  
  def outSpray(rdd:RDD[T])
  
   
  def postSpray(){
    
  }

}
