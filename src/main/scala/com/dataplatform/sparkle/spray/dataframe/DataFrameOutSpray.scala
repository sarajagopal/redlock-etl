package com.dataplatform.sparkle.spray.dataframe

import com.dataplatform.sparkle.spray.OutSpray
import com.dataplatform.sparkle.config.SparkleOutputConfig
import org.apache.spark.sql.DataFrame

abstract case class DataFrameOutSpray(sparkleOutputConfig: SparkleOutputConfig)
    extends OutSpray[DataFrame](sparkleOutputConfig)
    with Serializable {

  def preSpray() {}

  def outSpray(df: DataFrame)

  def postSpray() {}

}
