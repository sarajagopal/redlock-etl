package com.dataplatform.sparkle.spray.dataframe

import scala.collection.mutable._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.hive.HiveContext



/**
 * @author sarajagopal
 */
trait DataFrameInputProcessor {
  
   def readParquet(context:SQLContext, path:String):DataFrame ={
      context.read.parquet(path)
   }

   def readORC(context:SQLContext, path:String):DataFrame ={
      context.read.orc(path)
   }

  
    def readCSVFile(context:SQLContext, format: String, options: Map[String, String], filePath:String): DataFrame ={
     context.read.format(format).options(options).load(filePath)
   }
    
   
    def readJDBCTable(context:SQLContext,url:String, driver:String, table:String, userName:String, password:String): DataFrame ={
      val properties = new java.util.Properties
      properties.setProperty("user",userName)
      properties.setProperty("password",password)      
      context.read.option("driver", driver).jdbc(url, table, properties)
    }
    
    
   def readHive(hiveContext:HiveContext, sqlText:String): DataFrame ={
     hiveContext.sql(sqlText)
   }
   
   
//   def dropColumns(dataFrame: DataFrame,csvFileInputConfig:CSVFileInputConfig ):DataFrame = {
//     if(csvFileInputConfig.dropColumns != null){
//     val columns = csvFileInputConfig.dropColumns.split(",")
//     dropColumns(dataFrame,columns.toList)  
//     }else{
//       dataFrame
//     }
//     
//     
//   }
//   
//   def dropColumns(dataFrame: DataFrame,columns: List[String] ):DataFrame = {
//      columns.foldLeft(dataFrame){(dataFrame, column ) => dropColumns(dataFrame, column)}     
//   }
//   
//   def dropColumns(dataFrame: DataFrame, column :String ):DataFrame = {
//     dataFrame.drop(column)
//   }
//   
//   
//   def filterRows(dataFrame: DataFrame, csvFileInputConfig:CSVFileInputConfig ):DataFrame = { 
//     if(csvFileInputConfig.filterRows !=null) {
//       filterRows(dataFrame,csvFileInputConfig.filterRows)
//     }else{
//       dataFrame
//     }
//     
//    
//      
//   }
//   
//   def filterRows(dataFrame: DataFrame, filterMap :Map[String,String] ):DataFrame = {      
//      filterMap.foldLeft(dataFrame){(dataFrame, filterData) => filterRows(dataFrame,filterData._1, filterData._2) } 
//      
//   }
//    
//   
//    def filterRows(dataFrame: DataFrame, column:String, columnValue:String ):DataFrame = {
//        dataFrame.filter("\""+column+" != '"+columnValue+"'\"")
//    }
//  
//    
     
  
}