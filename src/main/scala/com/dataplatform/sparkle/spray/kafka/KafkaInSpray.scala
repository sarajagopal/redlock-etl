package com.dataplatform.sparkle.spray.kafka

import org.apache.spark.streaming.StreamingContext
import com.dataplatform.sparkle.spray.dstream.DStreamInSpray
import org.apache.spark.streaming.dstream.DStream



class KafkaInSpray(context: StreamingContext, kafkaInputConfig: KafkaInputConfig) extends DStreamInSpray(context, kafkaInputConfig) {

  def inSpray(): DStream[Nothing] = {
    null
  }
  
}