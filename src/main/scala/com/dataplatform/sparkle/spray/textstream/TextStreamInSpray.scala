package com.dataplatform.sparkle.spray.textstream

import org.apache.spark.streaming.StreamingContext
import com.dataplatform.sparkle.spray.dstream.DStreamInSpray
import org.apache.spark.streaming.dstream.DStream

class TextStreamInSpray(context: StreamingContext, textStreamInputConfig: TextStreamInputConfig) extends DStreamInSpray(context, textStreamInputConfig) {

  def inSpray(): DStream[Nothing] = {
    null
  }
 
  
}