package com.dataplatform.sparkle.spray

import com.dataplatform.sparkle.spray.rdd.RDDOutputProcessor
import com.dataplatform.sparkle.spray.dataframe.DataFrameOutputProcessor
import com.dataplatform.sparkle.spray.dstream.StreamOutputProcessor

/**
 * @author sarajagopal
 */
class SparkleOutputProcessor extends RDDOutputProcessor with DataFrameOutputProcessor with StreamOutputProcessor

object SparkleOutputProcessor extends SparkleOutputProcessor