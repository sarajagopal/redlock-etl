package com.dataplatform.sparkle.spray.orc

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
 * @author sarajagopal
 */
case class ORCInputConfig( sprayName: String,path: String) extends SparkleInputConfig