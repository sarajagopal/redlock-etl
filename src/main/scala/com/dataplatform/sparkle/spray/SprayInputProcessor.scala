package com.dataplatform.sparkle.spray

import com.dataplatform.sparkle.spray.dstream.DStreamInputProcessor
import com.dataplatform.sparkle.spray.rdd.RDDInputProcessor
import com.dataplatform.sparkle.spray.dataframe.DataFrameInputProcessor



/**
 * @author sarajagopal
 */
class SprayInputProcessor extends RDDInputProcessor with DStreamInputProcessor with  DataFrameInputProcessor {
  
}

object SprayInputProcessor extends SprayInputProcessor