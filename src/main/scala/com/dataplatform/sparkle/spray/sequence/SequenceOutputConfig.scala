package com.dataplatform.sparkle.spray.sequence

import com.dataplatform.sparkle.config.SparkleOutputConfig

case class SequenceOutputConfig(sprayName: String, path: String, partitons: Int)
    extends SparkleOutputConfig
