package com.dataplatform.sparkle.config

/**
 * @author sarajagopal
 */
case class SparkStreamingContextConfig(timeInterval: Int)
