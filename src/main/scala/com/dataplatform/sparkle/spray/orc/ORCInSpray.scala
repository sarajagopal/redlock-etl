package com.dataplatform.sparkle.spray.orc

import com.dataplatform.sparkle.spray.SprayInputProcessor
import org.apache.spark.sql.DataFrame
import com.dataplatform.sparkle.spray.dataframe.DataFrameInSpray
import com.dataplatform.sparkle.spray.parquet.ParquetInputConfig
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.hive.HiveContext

class ORCInSpray(context: HiveContext, orcInputConfig: ORCInputConfig) extends DataFrameInSpray(context, orcInputConfig) {

  def inSpray(): DataFrame = {
    SprayInputProcessor.readORC(context, orcInputConfig.path)
  }

}