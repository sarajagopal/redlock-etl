package com.dataplatform.sparkle.spray.textstream

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
 * @author sarajagopal
 */
case class TextStreamInputConfig(sprayName: String, directoryPath: String, storageLevel:String) extends SparkleInputConfig