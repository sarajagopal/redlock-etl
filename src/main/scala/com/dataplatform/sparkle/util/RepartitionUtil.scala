package com.dataplatform.sparkle.util

import org.apache.spark.sql.DataFrame
import org.apache.spark.rdd.RDD

object RepartitionUtil {

  def partitionDataFrame(partition: Int, df: DataFrame): DataFrame = {
    if (partition > 0) {
      df.repartition(partition)
    } else {
      df
    }
  }

  def partitionRDD[T](partition: Int, rdd: RDD[T]): RDD[T] = {
    if (partition > 0) {
      rdd.repartition(partition)
    } else {
      rdd
    }
  }

}
