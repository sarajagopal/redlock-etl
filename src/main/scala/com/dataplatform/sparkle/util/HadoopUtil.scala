package com.dataplatform.sparkle.util

import scala.collection.JavaConversions._

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import scala.io.Source.{fromInputStream}
import org.apache.hadoop.fs.FileStatus
import org.apache.hadoop.fs.RemoteIterator
import org.apache.hadoop.fs.LocatedFileStatus
import org.apache.spark.annotation.Experimental

@Experimental
class HadoopUtil(configuration: Configuration) extends Serializable {

  def getHadoopFileSystem(): FileSystem = {
    FileSystem.get(configuration)
  }

  def read(path: String): String = {
    val is = getHadoopFileSystem().open(new Path(path))
    fromInputStream(is).mkString
  }

  def write(path: String, contents: String) {
    val os = getHadoopFileSystem().create(new Path(path))
    os.write(contents.getBytes)
    os.flush()
    os.close()
  }

  def delete(path: String, recursive: Boolean) {
    getHadoopFileSystem().delete(new Path(path), recursive)

  }

  def move(currentPath: String, newPath: String) {
    getHadoopFileSystem().rename(new Path(currentPath), new Path(newPath))
  }

  def listStatus(path: String): Option[List[FileStatus]] = {
    getHadoopFileSystem().exists(new Path(path)) match {
      case true  => Some(getHadoopFileSystem().listStatus(new Path(path)).toList)
      case false => None
    }
  }

  def listFiles(path: String): Option[List[String]] = {
    listStatus(path) match {
      case Some(filestatuses: List[FileStatus]) =>
        Some(filestatuses.map { filestatus =>
          filestatus.getPath.toString()
        })
      case None => None
    }

  }

  def exists(pathToFind: String): Boolean = {
    getHadoopFileSystem().exists(new Path(pathToFind))
  }

  def listFiles(path: String,
                recursive: Boolean): RemoteIterator[LocatedFileStatus] = {
    getHadoopFileSystem().listFiles(new Path(path), recursive)
  }

}

object HadoopUtil {
  def apply(configuration: Configuration): HadoopUtil = {
    HadoopUtil(configuration)
  }
}
