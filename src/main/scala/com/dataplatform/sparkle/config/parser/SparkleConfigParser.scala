package com.dataplatform.sparkle.config.parser

import java.io.File
import java.io.InputStream
import com.dataplatform.sparkle.config.SparkleConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.dataplatform.sparkle.config.SparkleConfigException
import com.dataplatform.sparkle.util.HadoopUtil
import com.dataplatform.sparkle.util.AWSS3Util
import com.amazonaws.services.s3.AmazonS3Client
import org.apache.hadoop.conf.Configuration

trait SparkleConfigParser[T <: SparkleConfiguration] {

  def getObjectMapper(): ObjectMapper

  def parseConfiguartionFromString(conf: String,
                                   configurationClass: Class[T]): T = {
    getObjectMapper().readValue[T](conf, configurationClass)
  }
  def parseConfiguartionFromFile(file: File,
                                 configurationClass: Class[T]): T = {
    getObjectMapper().readValue[T](file, configurationClass)
  }

  def parseConfiguartionFromStream(stream: InputStream,
                                   configurationClass: Class[T]): T = {
    getObjectMapper().readValue[T](stream, configurationClass)
  }

  def parseConfiguartionFromHDFS(path: String,
                                 configuration: Configuration,
                                 configurationClass: Class[T]): T = {
    parseConfiguartionFromString(HadoopUtil(configuration).read(path),
                                 configurationClass)
  }

  def parseConfiguartionFromS3(path: String,
                               s3Client: AmazonS3Client,
                               configurationClass: Class[T]): T = {
    parseConfiguartionFromString(AWSS3Util(s3Client) getS3ObjectContent (path),
                                 configurationClass)
  }

  def parseConfiguartionFromS3(bucket: String,
                               key: String,
                               s3Client: AmazonS3Client,
                               configurationClass: Class[T]): T = {
    parseConfiguartionFromString(
      AWSS3Util(s3Client) getS3ObjectContent (bucket, key),
      configurationClass)
  }
}
