package com.dataplatform.sparkle.config

/**
 * @author sarajagopal
 */
case class SparkJobConf(applicationProperties: Map[String, String])