package com.dataplatform.sparkle.config.parser

import java.io.File
import java.io.InputStream
import com.dataplatform.sparkle.config.SparkleAppConfiguration
import org.apache.hadoop.conf.Configuration
import com.amazonaws.services.s3.AmazonS3Client

object SparkleAppConfigurationParser {

  val jsonParser = new SparkleConfigJSONParser[SparkleAppConfiguration]
  val yamlParser = new SparkleConfigYAMLParser[SparkleAppConfiguration]

  def parseJSONConfiguartionFromString(
      conf: String): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromString(conf,
                                            classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromString(
      conf: String): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromString(conf,
                                            classOf[SparkleAppConfiguration])
  }

  def parseJSONConfiguartionFromFile(file: File): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromFile(file,
                                          classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromFile(file: File): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromFile(file,
                                          classOf[SparkleAppConfiguration])
  }

  def parseJSONConfiguartionFromStream(
      stream: InputStream): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromStream(stream,
                                            classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromStream(
      stream: InputStream): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromStream(stream,
                                            classOf[SparkleAppConfiguration])
  }

  def parseJSONConfiguartionFromHDFS(
      path: String,
      configuration: Configuration): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromHDFS(path,
                                          configuration,
                                          classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromHDFS(
      path: String,
      configuration: Configuration): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromHDFS(path,
                                          configuration,
                                          classOf[SparkleAppConfiguration])
  }

  def parseJSONConfiguartionFromS3(
      path: String,
      s3Client: AmazonS3Client): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromS3(path,
                                        s3Client,
                                        classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromS3(
      path: String,
      s3Client: AmazonS3Client): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromS3(path,
                                        s3Client,
                                        classOf[SparkleAppConfiguration])
  }

  def parseYAMLConfiguartionFromS3(
      bucket: String,
      key: String,
      s3Client: AmazonS3Client): SparkleAppConfiguration = {
    yamlParser.parseConfiguartionFromS3(bucket,
                                        key,
                                        s3Client,
                                        classOf[SparkleAppConfiguration])
  }

  def parseJSONConfiguartionFromS3(
      bucket: String,
      key: String,
      s3Client: AmazonS3Client): SparkleAppConfiguration = {
    jsonParser.parseConfiguartionFromS3(bucket,
                                        key,
                                        s3Client,
                                        classOf[SparkleAppConfiguration])
  }

}
