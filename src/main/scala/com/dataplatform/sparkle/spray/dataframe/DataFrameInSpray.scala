package com.dataplatform.sparkle.spray.dataframe

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SQLContext
import com.dataplatform.sparkle.config.SparkleInputConfig
import com.dataplatform.sparkle.spray.InSpray

abstract case class DataFrameInSpray(context: SQLContext,
                                     sparkleInputConfig: SparkleInputConfig)
    extends InSpray[DataFrame](sparkleInputConfig)
    with Serializable {

  def preSpray() {}

  def inSpray(): DataFrame

  def postSpray() {}
}
