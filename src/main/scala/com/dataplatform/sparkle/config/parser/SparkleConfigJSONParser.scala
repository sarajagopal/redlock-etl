package com.dataplatform.sparkle.config.parser

import com.dataplatform.sparkle.config.SparkleConfiguration
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

/**
  * @author sarajagopal
  */
class SparkleConfigJSONParser[T <: SparkleConfiguration]
    extends SparkleConfigParser[T] {

  def getObjectMapper(): ObjectMapper = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL)
    mapper

  }

}
