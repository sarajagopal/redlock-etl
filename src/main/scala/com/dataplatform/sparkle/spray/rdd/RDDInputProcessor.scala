package com.dataplatform.sparkle.spray.rdd

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.hadoop.mapreduce.InputFormat

/**
 * @author sarajagopal
 */
trait RDDInputProcessor {
  
  def readText(context : SparkContext, path : String, partitons : Int) : RDD[String] = {
    if(partitons> 0){
    context.textFile(path, partitons)
    }else{
     context.textFile(path)
    }
  }
  
  def readSequenceFile[K, V](context : SparkContext,keyClass: Class[K],valueClass: Class[V],  path : String, partitons : Int) : RDD[(K, V)]  = {
    if(partitons> 0){
    context.sequenceFile(path,keyClass, valueClass, partitons)
    }else{
     context.sequenceFile(path, keyClass, valueClass)
    
    }
  }
  
  def readHadoopFile[K, V,  F <: InputFormat[K, V]](context : SparkContext, formatClass: Class[F], keyClass: Class[K], valueClass: Class[V],  path : String) : RDD[(K, V)]  = {
     context.newAPIHadoopFile(path, formatClass, keyClass, valueClass, context.hadoopConfiguration)     
  }
  
  
  
  

}