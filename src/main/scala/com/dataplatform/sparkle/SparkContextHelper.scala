package com.dataplatform.sparkle

import org.apache.spark._
import org.apache.spark.sql.SparkSession._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.sql.hive.HiveContext

/**
  * @author sarajagopal
  */
object SparkContextHelper {

  def createSparkContext(appName: String): SparkContext = {
    val sparkConf = new SparkConf()
    sparkConf.setAppName(appName)
    createSparkContext(sparkConf)
  }

  def createSparkContext(sparkConf: SparkConf): SparkContext = {
    new SparkContext(sparkConf)
  }

  def createStreamingContext(sparkConf: SparkConf,
                             timeInterval: Int): StreamingContext = {
    new StreamingContext(sparkConf, Seconds(timeInterval))
  }

  def createStreamingContext(context: SparkContext,
                             timeInterval: Int): StreamingContext = {
    new StreamingContext(context, Seconds(timeInterval))
  }

  def createSQLContextFromStreamingContext(
      ssc: StreamingContext): SQLContext = {
    SQLContext.getOrCreate(ssc.sparkContext)
  }

  def createSQLContextFromSparkContext(sc: SparkContext): SQLContext = {
    SQLContext.getOrCreate(sc)

  }

  def createHiveContextFromSparkContext(sc: SparkContext): HiveContext = {
    new HiveContext(sc)
  }

  def createSQLSession(sparkConf: SparkConf): SparkSession = {
    SparkSession
      .builder()
      .appName(sparkConf.get("appName", "Test"))
      .config(sparkConf)
      .getOrCreate()

  }

  def createHiveSession(sparkConf: SparkConf): SparkSession = {
    SparkSession
      .builder()
      .appName(sparkConf.get("appName", "Test"))
      .config(sparkConf)
      .enableHiveSupport()
      .getOrCreate()
  }

}
