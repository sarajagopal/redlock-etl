package com.dataplatform.sparkle.config.parser

import com.dataplatform.sparkle.config.SparkleConfiguration
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping

/**
  * @author sarajagopal
  */
class SparkleConfigYAMLParser[T <: SparkleConfiguration]
    extends SparkleConfigParser[T] {

  def getObjectMapper(): ObjectMapper = {
    val mapper = new ObjectMapper(new YAMLFactory())
    mapper.registerModule(DefaultScalaModule)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper

  }

}
