package com.dataplatform.sparkle.spray.csv
import scala.collection.mutable._
import com.dataplatform.sparkle.config.SparkleInputConfig

/**
  * @author sarajagopal
  */
case class CSVInputConfig(sprayName: String,
                          format: String = "com.databricks.spark.csv",
                          header: Boolean = false,
                          delimiter: Char = ',',
                          quote: Char = '"',
                          escape: Char = '\\',
                          parserLib: String = "commons",
                          mode: String = "PERMISSIVE",
                          charset: String = "UTF-8",
                          comment: Char = '#',
                          dropColumns: String,
                          filterRows: Map[String, String],
                          filePath: String)
    extends SparkleInputConfig {

  def getOptions(): Map[String, String] = {
    Map(
      "format" -> format,
      "header" -> header.toString(),
      "delimiter" -> delimiter.toString(),
      "quote" -> quote.toString(),
      "parserLib" -> parserLib,
      "mode" -> mode,
      "charset" -> charset,
      "comment" -> comment.toString()
    )
  }

}
