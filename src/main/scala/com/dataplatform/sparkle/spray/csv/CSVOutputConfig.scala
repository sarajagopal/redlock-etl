package com.dataplatform.sparkle.spray.csv

import org.apache.spark.sql.SaveMode
import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
 * @author sarajagopal
 */
case class CSVOutputConfig(sprayName: String,
                           format: String = "com.databricks.spark.csv",
                           header: Option[Boolean],
                           delimiter: Option[Char],
                           quote: Option[Char],
                           escape: Option[Char],
                           comment: Option[Char],
                           filePath: String,
                           saveMode: SaveMode = SaveMode.Overwrite,
                           rePartition: Int) extends SparkleOutputConfig {

  def getOptions(): Map[String, String] = {
    Map("header" -> header.getOrElse(false).toString(), "delimiter" -> delimiter.getOrElse(',').toString(), "quote" -> quote.getOrElse('"').toString(),
      "escape" -> escape.getOrElse('\\').toString(), "comment" -> comment.getOrElse('#').toString())
  }

}