package com.dataplatform.sparkle.spray.jdbc

import org.apache.spark.sql.DataFrame
import com.dataplatform.sparkle.spray.dataframe.DataFrameInSpray
import com.dataplatform.sparkle.spray.csv.CSVInputConfig
import org.apache.spark.sql.SQLContext
import com.dataplatform.sparkle.spray.SprayInputProcessor

class JdbcInSpray(context: SQLContext, jdbcInputConfig: JdbcInputConfig)
    extends DataFrameInSpray(context, jdbcInputConfig) {

  def inSpray(): DataFrame = {
    SprayInputProcessor.readJDBCTable(context,
                                      jdbcInputConfig.url,
                                      jdbcInputConfig.driver,
                                      jdbcInputConfig.table,
                                      jdbcInputConfig.userName,
                                      jdbcInputConfig.password)
  }

}
