package com.dataplatform.sparkle.spray.dataframe

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import com.dataplatform.sparkle.util.RepartitionUtil

/**
 * @author sarajagopal
 */
trait DataFrameOutputProcessor {

  def writeORCFile(df: DataFrame, directoryPath: String, saveMode: String, rePartition: Int) {
    RepartitionUtil.partitionDataFrame(rePartition, df).write.mode(saveMode).orc(directoryPath)
  }

  def writeParquetFile(df: DataFrame, directoryPath: String, saveMode: String, rePartition: Int) {
    RepartitionUtil.partitionDataFrame(rePartition, df).write.mode(saveMode).parquet(directoryPath)
  }

  def writeJDBCTable(df: DataFrame, url: String, driver: String, table: String, userName: String, password: String, saveMode: String) {
    val prop = new java.util.Properties
    prop.setProperty("user", userName)
    prop.setProperty("password", password)
    df.write.mode(saveMode).option("driver", driver).jdbc(url, table, prop)
  }

  def writeCSVFile(df: DataFrame, format: String, options: Map[String, String], saveMode: SaveMode, filePath: String, rePartition: Int) {
    RepartitionUtil.partitionDataFrame(rePartition, df).write.mode(saveMode).format(format).options(options).save(filePath)
  }

}