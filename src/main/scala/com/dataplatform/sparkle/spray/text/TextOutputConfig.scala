package com.dataplatform.sparkle.spray.text

import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
  * @author sarajagopal
  */
case class TextFileOutputConfig(sprayName: String,
                                directoryPath: String,
                                rePartition: Int)
    extends SparkleOutputConfig
