package com.dataplatform.sparkle.spray.hadoop

import com.dataplatform.sparkle.config.SparkleInputConfig
import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
 * @author sarajagopal
 */
case class HadoopOutputConfig(val sprayName: String,var path: String, var partitons: Int, var hadoopKeyType: String, var hadoopValueType: String, var inputFormat: String)  extends SparkleOutputConfig