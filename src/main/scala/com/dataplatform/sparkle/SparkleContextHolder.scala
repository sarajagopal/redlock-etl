package com.dataplatform.sparkle

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.streaming.StreamingContext
import com.dataplatform.sparkle.config.SparkleConfiguration
import com.dataplatform.sparkle.configuration.util.HadoopConfigurationHelper

/**
  * @author sarajagopal
  */
case class SparkleContextHolder(sparkContext: SparkContext,
                                sqlContext: SQLContext,
                                sparkSession: SparkSession,
                                streamingContext: Option[StreamingContext])
    extends Serializable

object SparkleContextHolder {

  def apply(configuration: SparkleConfiguration): SparkleContextHolder = {

    val sparkConf =
      new SparkConf().setAll(configuration.sparkJobConf.applicationProperties)

    val sparkSession = createSQLOrHiveContext(configuration, sparkConf)

    val streamingContext = createSparkStreamingContextContext(
      configuration,
      sparkSession.sparkContext)

    SparkleContextHolder(sparkSession.sparkContext,
                         sparkSession.sqlContext,
                         sparkSession,
                         streamingContext)

  }

  def createSQLOrHiveContext(configuration: SparkleConfiguration,
                             sparkConf: SparkConf): SparkSession = {
    val sparkSession =
      if (configuration.sparkContextConfig.createHiveContext)
        SparkContextHelper.createHiveSession(sparkConf)
      else {
        SparkContextHelper.createSQLSession(sparkConf)
      }
    addConfigToSparkContext(configuration: SparkleConfiguration,
                            sparkSession.sparkContext)
    sparkSession
  }

  def addConfigToSparkContext(configuration: SparkleConfiguration,
                              sparkContext: SparkContext) {
    if (configuration.sparkContextConfig.checkPointDirectory.isDefined)
      sparkContext.setCheckpointDir(
        configuration.sparkContextConfig.checkPointDirectory.get)
    HadoopConfigurationHelper.addHadoopConfiguration(
      sparkContext,
      configuration.hadoopConfiguration)
  }

  def createSparkStreamingContextContext(
      configuration: SparkleConfiguration,
      sparkContext: SparkContext): Option[StreamingContext] = {
    if (configuration.sparkContextConfig.sparkStreamingContextConfig.isDefined)
      Some(
        SparkContextHelper.createStreamingContext(
          sparkContext,
          configuration.sparkContextConfig.sparkStreamingContextConfig.get.timeInterval))
    else None
  }

}
