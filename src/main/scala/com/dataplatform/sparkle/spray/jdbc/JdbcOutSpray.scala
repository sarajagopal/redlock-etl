package com.dataplatform.sparkle.spray.jdbc

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.DataFrame
import com.dataplatform.sparkle.spray.OutSpray
import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import com.dataplatform.sparkle.spray.dataframe.DataFrameOutSpray

class JdbcOutSpray(jdbcOutputConfig: JdbcOutputConfig)
    extends DataFrameOutSpray(jdbcOutputConfig) {

  def outSpray(df: DataFrame) {
    SparkleOutputProcessor.writeJDBCTable(df,
                                          jdbcOutputConfig.url,
                                          jdbcOutputConfig.driver,
                                          jdbcOutputConfig.table,
                                          jdbcOutputConfig.userName,
                                          jdbcOutputConfig.password,
                                          jdbcOutputConfig.saveMode)
  }

}
