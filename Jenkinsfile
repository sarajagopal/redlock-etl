#!groovy​

// Execute!
main()

// Execution list
def main() {
    try {
        node(label: 'worker') {
            try {
                deleteDir()
                checkout scm
                compile()
                test()
            } finally {
                removeBuildDirectory()
            }
        }
        // run manual input step off node so it doesn't block
        if (approvePublish()) {
            node(label: 'worker') {
                try {
                    deleteDir()
                    checkout scm
                    jar()
                    dockerize()
                    publish()
                } finally {
                    removeImages()
                    removeBuildDirectory()
                }
            }
        }
        bitbucketStatusNotify(buildState: 'SUCCESSFUL')
    } catch (err) {
        bitbucketStatusNotify(buildState: 'FAILED')
    }
}

// Helper function
def gitCommitHash() {
    sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
}

// Stages defined as functions
def compile() {
    stage('Compile') {
        sh './gradlew clean testClasses --no-daemon'
    }
}

def test() {
    stage('Test') {
        try {
            sh './gradlew test --no-daemon'
        } finally {
            junit '**/build/test-results/test/*.xml'
        }
    }
}

boolean approvePublish() {
    try {
        stage('Publish?') {
            timeout(time: 3, unit: 'MINUTES') {
                input message: 'Approve publishing?'
            }
        }
        true
    } catch (err) { // catch timeout or abort
        currentBuild.result = 'SUCCESS'
        false
    }
}

def jar() {
    stage('Jar') {
        sh './gradlew clean bootJar --no-daemon'
    }
}

def dockerize() {
    stage('Dockerize') {
        withEnv(["GIT_COMMIT_HASH=${gitCommitHash()}",
                 "DOCKER_HOST=127.0.0.1:2375",
                 "DOCKER_NETWORK=redlock" ]) {

            sh 'docker-compose -f docker/docker-compose.yml build'
        }
    }
}

def publish() {
    stage('Publish') {
        lock('docker-hub') {
            withDockerRegistry(credentialsId: 'b9b4c76d-fa47-4586-8560-3128fb557637', url: 'https://index.docker.io/v1/') {
                withEnv(["GIT_COMMIT_HASH=${gitCommitHash()}",
                         "DOCKER_HOST=127.0.0.1:2375",
                         "DOCKER_NETWORK=redlock"]) {

                    sh 'docker-compose -f docker/docker-compose.yml push'
                }
            }
        }
    }
}

def removeImages() {
    stage('Cleanup images') {
        withEnv(["GIT_COMMIT_HASH=${gitCommitHash()}",
                 "DOCKER_HOST=127.0.0.1:2375",
                 "DOCKER_NETWORK=redlock"]) {

            sh 'docker-compose -f docker/docker-compose.yml down --rmi all'
        }
    }
}

def removeBuildDirectory() {
    stage('Cleanup build directory') {
        sh "rm -fr $env.WORKSPACE/$env.BUILD_NUMBER $env.WORKSPACE/$env.BUILD_NUMBER@tmp"
    }
}
