package com.dataplatform.sparkle.spray.rdd

import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.util.RepartitionUtil
import org.apache.hadoop.io.Writable
import org.apache.spark.rdd.RDD.rddToSequenceFileRDDFunctions
import org.apache.hadoop.io.compress.CompressionCodec



/**
 * @author sarajagopal
 */
trait RDDOutputProcessor {
  
  def writeText(rdd:RDD[String], path:String,rePartition:Int ){
    RepartitionUtil.partitionRDD(rePartition, rdd).saveAsTextFile(path)  
  }
  
  def writeSequence(rdd:RDD[(Writable, Writable)], path:String,rePartition:Int, compressionCodec : Class[_ <: CompressionCodec] ){
    RepartitionUtil.partitionRDD(rePartition, rdd).saveAsSequenceFile(path, Some(compressionCodec))
  }
  
}