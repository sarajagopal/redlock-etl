package com.dataplatform.sparkle.configuration.util

import org.apache.spark.SparkContext

/**
 * @author sarajagopal
 */
object HadoopConfigurationHelper {

  def addHadoopConfiguration(context: SparkContext, hadoopConfiguration: Map[String, String]) {
    if (hadoopConfiguration != null) hadoopConfiguration.foreach(configTuple => context.hadoopConfiguration.set(configTuple._1, configTuple._2))
  }

  def addHadoopConfiguration(context: SparkContext, key: String, value: String) {
    context.hadoopConfiguration.set(key, value)
  }

}