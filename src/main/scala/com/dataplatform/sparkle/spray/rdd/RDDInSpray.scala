package com.dataplatform.sparkle.spray.rdd

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.config.SparkleInputConfig
import com.dataplatform.sparkle.spray.InSpray

abstract class RDDInSpray[T](context: SparkContext, sparkleInputConfig: SparkleInputConfig) extends InSpray[RDD[T]](sparkleInputConfig) with Serializable { 
   
  def preSpray(){    
  }
 
  def inSpray():RDD[T]
  
    
  def postSpray(){    
  }
}