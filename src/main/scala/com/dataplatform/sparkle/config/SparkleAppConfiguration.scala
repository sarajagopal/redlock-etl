package com.dataplatform.sparkle.config

class SparkleAppConfiguration(val sparkJobConf: SparkJobConf,
                              val sparkContextConfig: SparkContextConfig,
                              val hadoopConfiguration: Map[String, String],
                              val yarnConfiguration: Map[String, String])
    extends SparkleConfiguration
