package com.dataplatform.sparkle.spray.csv

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SQLContext
import com.dataplatform.sparkle.spray.dataframe.DataFrameInSpray
import com.dataplatform.sparkle.spray.SprayInputProcessor

class CSVInSpray(context: SQLContext, csvFileInputConfig: CSVInputConfig)
    extends DataFrameInSpray(context, csvFileInputConfig) {

  def inSpray(): DataFrame = {
    SprayInputProcessor.readCSVFile(context,
                                    csvFileInputConfig.format,
                                    csvFileInputConfig.getOptions(),
                                    csvFileInputConfig.filePath)
  }

}
