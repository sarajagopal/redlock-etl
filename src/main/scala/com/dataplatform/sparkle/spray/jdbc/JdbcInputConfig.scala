package com.dataplatform.sparkle.spray.jdbc

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
 * @author sarajagopal
 */
case class JdbcInputConfig( sprayName: String, url: String,  driver: String,  table: String,  userName: String,  password: String) extends SparkleInputConfig