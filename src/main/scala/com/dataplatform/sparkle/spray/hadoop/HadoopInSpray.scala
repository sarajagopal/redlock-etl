package com.dataplatform.sparkle.spray.hadoop

import com.dataplatform.sparkle.spray.sequence.SequenceInputConfig
import com.dataplatform.sparkle.spray.rdd.RDDInSpray
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.spray.SprayInputProcessor
import org.apache.hadoop.io.Writable
import org.apache.hadoop.mapreduce.InputFormat
import com.dataplatform.sparkle.util.ReflectionUtil

class HadoopInSpray [K <: Writable,V <: Writable, F <: InputFormat[K, V]](context: SparkContext, hadoopInputConfig: HadoopInputConfig) extends RDDInSpray[(K, V)](context, hadoopInputConfig) {

  def inSpray():RDD[(K, V)] =   {
    val classLoader  = getClass.getClassLoader
    val hadoopFormatClass = ReflectionUtil.string2Class[F](hadoopInputConfig.inputFormat)(classLoader)
    val keyClass = ReflectionUtil.string2Class[K](hadoopInputConfig.hadoopKeyType)(classLoader)
    val valueClass = ReflectionUtil.string2Class[V](hadoopInputConfig.hadoopValueType)(classLoader)     
    SprayInputProcessor.readHadoopFile(context, hadoopFormatClass, keyClass, valueClass, hadoopInputConfig.path)
  }  
  
  

  
}