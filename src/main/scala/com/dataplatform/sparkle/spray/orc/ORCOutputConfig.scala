package com.dataplatform.sparkle.spray.orc

import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
 * @author sarajagopal
 */
case class ORCOutputConfig(sprayName: String, directoryPath: String,  rePartition: Int,  saveMode: String) extends SparkleOutputConfig