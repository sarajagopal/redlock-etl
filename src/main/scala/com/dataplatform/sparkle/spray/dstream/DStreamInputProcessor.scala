package com.dataplatform.sparkle.spray.dstream

import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

/**
 * @author sarajagopal
 */
trait DStreamInputProcessor {
  
  def processTextStreamInput(context:StreamingContext, path:String,storageLevel: StorageLevel ):DStream[String]= {
     context.textFileStream(path).persist(storageLevel)
  }
  

  
  
}