package com.dataplatform.sparkle.spray

import com.dataplatform.sparkle.config.SparkleOutputConfig

abstract class  OutSpray[T](sparkleOutputConfig: SparkleOutputConfig){
  
  def preSpray() 
  
  
  def outSpray(t:T)
  
  def execute(t:T){
    preSpray()
    outSpray(t)
    postSpray()
  } 
  
  def postSpray() 
  
  
}