package com.dataplatform.sparkle.spray.akka

import com.dataplatform.sparkle.spray.dstream.DStreamInSpray
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream

class AkkaInSpray (context: StreamingContext, akkaInputConfig: AkkaInputConfig) extends DStreamInSpray(context, akkaInputConfig) {

  def inSpray(): DStream[Nothing] = {
    null
  }
  
}