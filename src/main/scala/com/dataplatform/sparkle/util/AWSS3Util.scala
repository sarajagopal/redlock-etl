package com.dataplatform.sparkle.util

import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.util.ArrayList

import scala.collection.JavaConversions.asScalaBuffer

import com.amazonaws.AmazonWebServiceClient
import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.CopyObjectRequest
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.DeleteObjectsRequest
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.amazonaws.services.s3.model.CopyPartResult
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest
import com.amazonaws.services.s3.model.GetObjectMetadataRequest
import com.amazonaws.services.s3.model.CopyPartRequest
import com.amazonaws.services.s3.model.CompleteMultipartUploadResult
import com.amazonaws.services.s3.model.PartETag
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest
import scala.collection.JavaConverters._

class AWSS3Util(s3Client: AmazonS3Client) extends Serializable {

  val S3_PROTOCOL = "s3"

  def checkIfS3ObjectExists(s3URL: String): Boolean = {
    val (bucket, key) = getBucketAndKey(s3URL)
    checkIfS3ObjectExists(bucket, key)
  }

  def checkIfS3ObjectExists(bucket: String, key: String): Boolean = {
    println(
      s"Checking if  S3 Object Exists in bucket [ $bucket] and key [ $key ]")
    val result = checkIfS3FileExists(bucket, key) || checkIfS3FolderExists(
      bucket,
      key)
    result match {
      case true =>
        println(s"Found  S3  Object in bucket [ $bucket] and key [ $key ]")
      case false =>
        println(
          s"Did Not Found any S3  Object in bucket [ $bucket] and key [ $key ]")
    }
    result
  }

  def checkIfS3FileExists(s3URL: String): Boolean = {
    val (bucket, key) = getBucketAndKey(s3URL)
    checkIfS3ObjectExists(bucket, key)
  }

  def checkIfS3FolderExists(s3URL: String): Boolean = {
    val (bucket, key) = getBucketAndKey(s3URL)
    checkIfS3FolderExists(bucket, key)
  }

  def checkIfS3FolderExists(bucket: String, key: String): Boolean = {
    println(
      s"Checking if  S3 Folder Exists in bucket [ $bucket] and key [ $key ]")
    try {
      val response = s3Client.listObjects(
        new ListObjectsRequest().withBucketName(bucket).withPrefix(key))
      if (response.getObjectSummaries.length > 0) {
        !(response.getObjectSummaries.filter { s3ObjectSummary =>
          s3ObjectSummary.getBucketName.equals(bucket) || s3ObjectSummary.getKey
            .equals(s"$key")
        }.isEmpty)
      } else {
        false
      }

    } catch {
      case e: Exception =>
        println(
          s"Could not check If S3 Folder Exists in bucket [ $bucket] and key [ $key ]")
        false
    }
  }

  def checkIfS3FileExists(bucket: String, key: String): Boolean = {
    println(
      s"Checking if  S3 File Exists in bucket [ $bucket] and key [ $key ]")
    try {
      val metadata = s3Client.getObjectMetadata(bucket, key)
      true
    } catch {
      case e: Exception =>
        println(
          s"Could not check If S3 File Exists in bucket [ $bucket] and key [ $key ]")
        false
    }
  }

  def checkIfS3BucketExists(bucketName: String): Boolean = {
    try {
      s3Client.doesBucketExist(bucketName)
    } catch {
      case e: Exception =>
        println(s"Could not find bucket  [ $bucketName ]")
        false
    }
  }

  def getS3Object(s3URL: String): S3Object = {
    val (bucket, key) = getBucketAndKey(s3URL)
    getS3Object(bucket, key)
  }

  def getS3Object(bucket: String, key: String): S3Object = {
    try {
      s3Client.getObject(new GetObjectRequest(bucket, key))
    } catch {
      case e: Exception =>
        println(
          s"Could not find S3Object in bucket [ $bucket  ] and key [ $key ]")
        throw new Exception(
          s"Could not find S3Object in bucket [ $bucket  ] and key [ $key ]",
          e)
    }
  }

  def getS3ObjectContent(s3URL: String): String = {
    try {
      val (bucket, key) = getBucketAndKey(s3URL)
      getS3ObjectContent(bucket, key)
    } catch {
      case e: Exception =>
        println(s"Could not Read S3Object in s3URL [ $s3URL ] ")
        throw new Exception(s"Could not Read S3Object in bucket [ $s3URL]", e)
    }
  }

  def getS3ObjectContent(bucket: String, key: String): String = {
    try {
      scala.io.Source
        .fromInputStream(
          s3Client
            .getObject(new GetObjectRequest(bucket, key))
            .getObjectContent())
        .mkString
    } catch {
      case e: Exception =>
        println(
          s"Could not Read S3Object in bucket [ $bucket  ] and key [ $key ]")
        throw new Exception(
          s"Could not Read S3Object in bucket [ $bucket  ] and key [ $key ]",
          e)
    }
  }

  def listAllS3BucketAndKeys(s3URL: String): List[(String, String)] = {
    val s3ObjectSummaryList = listAllS3ObjectSummary(s3URL)
    s3ObjectSummaryList.map { s3objectSummary =>
      (s3objectSummary.getBucketName, s3objectSummary.getKey)
    }.toList
  }

  def listAllS3BucketAndKeys(bucket: String,
                             prefix: String = ""): List[(String, String)] = {
    val s3ObjectSummaryList = listAllS3ObjectSummary(bucket, prefix)
    s3ObjectSummaryList.map { s3objectSummary =>
      (s3objectSummary.getBucketName, s3objectSummary.getKey)
    }.toList
  }

  def listAllS3ObjectPaths(s3URL: String): List[String] = {
    val (bucket, key) = getBucketAndKey(s3URL)
    val s3ObjectSummaryList = listAllS3ObjectSummary(bucket, key)
    s3ObjectSummaryList.map { s3objectSummary =>
      createS3URL(S3_PROTOCOL,
                  s3objectSummary.getBucketName,
                  s3objectSummary.getKey)
    }.toList
  }

  def listAllS3ObjectSummary(s3URL: String): java.util.List[S3ObjectSummary] = {
    val (bucket, key) = getBucketAndKey(s3URL)
    listAllS3ObjectSummary(bucket, key)
  }

  def listAllS3ObjectSummary(
      bucket: String,
      prefix: String = ""): java.util.List[S3ObjectSummary] = {
    try {
      s3Client
        .listObjects(
          new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix))
        .getObjectSummaries()
    } catch {
      case e: Exception =>
        println(
          s"Could not listObjects in bucket [ $bucket ]  and with prefix [ $prefix ]")
        throw new Exception(
          s"Could not listObjects in bucket [ $bucket ]  and with prefix [ $prefix ]",
          e)
    }
  }

  def listCommonPrefixes(s3URL: String): List[(String, String)] = {
    val (bucket, key) = getBucketAndKey(s3URL)
    listCommonPrefixes(bucket, key)
  }

  def listCommonPrefixPaths(s3URL: String): List[String] = {
    val (bucket, key) = getBucketAndKey(s3URL)
    listCommonPrefixes(bucket, key).map {
      case (commonBucket, prefixKey) =>
        createS3URL(S3_PROTOCOL, commonBucket, prefixKey)
    }
  }

  def listCommonPrefixes(bucket: String,
                         prefix: String = ""): List[(String, String)] = {
    try {
      s3Client
        .listObjects(
          new ListObjectsRequest()
            .withBucketName(bucket)
            .withPrefix(prefix)
            .withDelimiter("/"))
        .getCommonPrefixes
        .toList
        .map { prefix =>
          (bucket, prefix)
        }

    } catch {
      case e: Exception =>
        println(
          s"Could not listObjects in bucket [ $bucket ]  and with prefix [ $prefix ]")
        throw new Exception(
          s"Could not listObjects in bucket [ $bucket ]  and with prefix [ $prefix ]",
          e)
    }
  }

  def getInputStreamForS3Object(s3URL: String): S3ObjectInputStream = {
    val (bucket, key) = getBucketAndKey(s3URL)
    getInputStreamForS3Object(bucket, key)
  }

  def getInputStreamForS3Object(bucket: String,
                                key: String): S3ObjectInputStream = {
    try {
      getS3Object(bucket, key).getObjectContent()
    } catch {
      case e: Exception =>
        println(
          s"Could not get InputStream For S3Object in bucket [ $bucket  ] and key [ $key ]")
        throw new Exception(
          s"Could not get InputStream For S3Object in bucket [ $bucket  ] and key [ $key ]",
          e)
    }

  }

  def putS3Object(putObjectRequest: PutObjectRequest) {
    s3Client.putObject(putObjectRequest)
  }

  def putS3Object(bucket: String, key: String, content: String) {
    val contentAsBytes = content.getBytes("UTF-8")
    val contentsAsStream = new ByteArrayInputStream(contentAsBytes)
    val md = new ObjectMetadata()
    md.setContentLength(contentAsBytes.length)
    putS3Object(bucket, key, contentsAsStream, md)
  }

  def putS3Object(bucket: String,
                  key: String,
                  contentsAsStream: InputStream,
                  objectMetadata: ObjectMetadata) {
    putS3Object(
      new PutObjectRequest(bucket, key, contentsAsStream, objectMetadata))
  }

  def putS3Object(bucket: String, key: String, file: File) {
    putS3Object(new PutObjectRequest(bucket, key, file))
  }

  def putS3Object(s3URL: String, file: File) {
    val (bucket, key) = getBucketAndKey(s3URL)
    putS3Object(new PutObjectRequest(bucket, key, file))
  }

  def putS3Object(s3URL: String, content: String) {
    val (bucket, key) = getBucketAndKey(s3URL)
    putS3Object(bucket, key, content)
  }

  def moveS3Objects(s3CurrentURL: String,
                    s3NewURL: String,
                    recursive: Boolean) {
    val (currBucket, currKey) = getBucketAndKey(s3CurrentURL)
    val (newBucket, newKey) = getBucketAndKey(s3NewURL)
    moveS3Objects(currBucket, currKey, newBucket, newKey, recursive)
  }

  def moveS3Objects(sourceBucket: String,
                    sourceKey: String,
                    targetBucket: String,
                    targetKey: String,
                    recursive: Boolean) {
    try {
      copyS3Objects(sourceBucket, sourceKey, targetBucket, targetKey, recursive)
      deleteS3Objects(sourceBucket, sourceKey, recursive)
    } catch {
      case e: AmazonS3Exception =>
        println("Couldnt move the data between Buckets" + e)
        throw new Exception("Couldnt move the data between Buckets", e)
    }

  }

  def copyS3Objects(sourceBucket: String,
                    sourceKey: String,
                    targetBucket: String,
                    targetKey: String,
                    recursive: Boolean) {
    recursive match {
      case true =>
        val sourceKeys = listAllS3BucketAndKeys(sourceBucket, sourceKey).map {
          case (bucket, key) => key
        }
        copyMulipleS3Objects(sourceBucket, sourceKeys, targetBucket, targetKey)
      case false =>
        copyS3Object(
          new CopyObjectRequest(sourceBucket,
                                sourceKey,
                                targetBucket,
                                targetKey))
    }

  }

  def copyMulipleS3Objects(sourceBucket: String,
                           sourceKey: List[String],
                           targetBucket: String,
                           targetKey: String) {
    sourceKey.map { sourceKey =>
      copyS3Object(
        new CopyObjectRequest(sourceBucket, sourceKey, targetBucket, targetKey))
    }
  }

  def copyMultiplePartsOfS3Object(sourceBucket: String,
                                  sourceKey: String,
                                  targetBucket: String,
                                  targetKey: String) {
    val copyPartResultList = new ArrayList[CopyPartResult]()
    val initiateRequest =
      new InitiateMultipartUploadRequest(targetBucket, targetKey)
    val initResult = s3Client.initiateMultipartUpload(initiateRequest)
    val uploadId = initResult.getUploadId()

    try {
      val metadataRequest =
        new GetObjectMetadataRequest(sourceBucket, sourceKey)
      val metadataResult = s3Client.getObjectMetadata(metadataRequest)
      val objectSize = metadataResult.getContentLength()

      val partSize = 5 * Math.pow(2.0, 20.0).toLong // 5 MB

      var bytePosition = 0L

      for (counter <- 0 until Int.MaxValue; if bytePosition < objectSize) {

        val lastbyte =
          if ((bytePosition + partSize - 1) >= objectSize) objectSize - 1
          else bytePosition + partSize - 1
        val copyRequest = new CopyPartRequest()
          .withDestinationBucketName(targetBucket)
          .withDestinationKey(targetKey)
          .withSourceBucketName(sourceBucket)
          .withSourceKey(sourceKey)
          .withUploadId(initResult.getUploadId())
          .withFirstByte(bytePosition)
          .withLastByte(lastbyte)
          .withPartNumber(counter)
        copyPartResultList.add(s3Client.copyPart(copyRequest));
        bytePosition = bytePosition + partSize
      }

      val completeRequest = new CompleteMultipartUploadRequest(
        targetBucket,
        targetKey,
        initResult.getUploadId(),
        GetETags(copyPartResultList).asJava);
      val completeUploadResponse =
        s3Client.completeMultipartUpload(completeRequest);
    } catch {
      case e: Exception => println(e.getMessage())

    }

  }

  def copyS3Object(copyObjectRequest: CopyObjectRequest) {
    s3Client.copyObject(copyObjectRequest)
  }

  def deleteS3Objects(s3URL: String, recursive: Boolean = false) {
    val (bucket, key) = getBucketAndKey(s3URL)
    deleteS3Objects(bucket, key, recursive)
  }

  def deleteS3Objects(bucket: String, key: String, recursive: Boolean) {
    recursive match {
      case true =>
        val keys = listAllS3BucketAndKeys(bucket, key).map {
          case (bucket, key) => key
        }
        deleteMulipleS3Object(bucket, keys)
      case false => deleteS3Object(new DeleteObjectRequest(bucket, key))
    }

  }

  def deleteMulipleS3Object(bucket: String, keys: List[String]) {
    val multiObjectDeleteRequest = new DeleteObjectsRequest(bucket)
    val keyversions = new ArrayList[KeyVersion]()

    keys.map { key =>
      keyversions.add(new KeyVersion(key))
    }

    multiObjectDeleteRequest.setKeys(keyversions)
    s3Client.deleteObjects(multiObjectDeleteRequest)
  }

  def deleteS3Object(deleteObjectRequest: DeleteObjectRequest) {
    s3Client.deleteObject(deleteObjectRequest)
  }

  def getBucketAndKey(s3URL: String): (String, String) = {
    val regex = "^s3[an]?://([^/]*?)/(.*)$".r

    s3URL match {
      case regex(bucket, key) => (bucket, key)
      case regex(bucket, _)   => (bucket, "")
      case _ =>
        println(s"Could not find bucket and key in $s3URL")
        throw new IllegalArgumentException(s"Invalid S3 URL: $s3URL")
    }

  }

  def createS3URL(s3Protocol: String, bucket: String, key: String): String = {
    s"$s3Protocol://$bucket/$key"
  }

  def GetETags(responses: ArrayList[CopyPartResult]): List[PartETag] = {
    responses.toList.map { response =>
      (new PartETag(response.getPartNumber(), response.getETag()))
    }
  }

}

object AWSS3Util {
  def apply(s3Client: AmazonS3Client): AWSS3Util = {
    new AWSS3Util(s3Client)
  }

}

class AWSClientBuilder extends Serializable {
  private var awsCredentialsProvider: AWSCredentialsProvider =
    new DefaultAWSCredentialsProviderChain()
  private var configuration = new ClientConfiguration()
  private var region: Option[Region] = Some(Region.getRegion(Regions.US_EAST_1))
  private var endpoint: Option[String] = None
  private var awsCredentials: Option[AWSCredentials] = None

  def getAWSS3Client(): AmazonS3Client = {
    val client = if (awsCredentials.isDefined) {
      new AmazonS3Client(awsCredentials.get, configuration)
    } else {
      new AmazonS3Client(awsCredentialsProvider, configuration)
    }
    setRegionAndEndPoint(client)
    client
  }

  private def setRegionAndEndPoint(
      client: AmazonWebServiceClient): AmazonWebServiceClient = {
    if (region.isDefined) {
      client.setRegion(region.get);
    }

    if (endpoint.isDefined) {
      client.setEndpoint(endpoint.get);
    }

    client

  }

  def withAWSCredentialsProvider(
      awsCredentialsProvider: AWSCredentialsProvider): AWSClientBuilder = {
    this.awsCredentialsProvider = awsCredentialsProvider
    this
  }

  def withAWSCredentials(awsCredentials: AWSCredentials): AWSClientBuilder = {
    this.awsCredentials = Some(awsCredentials)
    this
  }

  def withConfiguration(
      configuration: ClientConfiguration): AWSClientBuilder = {
    this.configuration = configuration
    this
  }

  def withRegion(region: Region): AWSClientBuilder = {
    this.region = Some(region)
    this
  }

  def withEndpoint(endpoint: String): AWSClientBuilder = {
    this.endpoint = Some(endpoint)
    this
  }

}
