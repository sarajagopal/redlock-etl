package com.dataplatform.sparkle.spray.text

import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import org.apache.spark.SparkContext
import com.dataplatform.sparkle.spray.rdd.RDDOutSpray
import org.apache.spark.rdd.RDD

class TextOutSpray(textFileOutputConfig: TextFileOutputConfig)
    extends RDDOutSpray[String](textFileOutputConfig) {

  def outSpray(rdd: RDD[String]) {
    SparkleOutputProcessor.writeText(rdd,
                                     textFileOutputConfig.directoryPath,
                                     textFileOutputConfig.rePartition)
  }

}
