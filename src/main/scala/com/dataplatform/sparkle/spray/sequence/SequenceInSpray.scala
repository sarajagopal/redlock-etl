package com.dataplatform.sparkle.spray.sequence

import com.dataplatform.sparkle.spray.rdd.RDDInSpray
import org.apache.spark.SparkContext
import com.dataplatform.sparkle.spray.text.TextInputConfig
import org.apache.spark.rdd.RDD
import com.dataplatform.sparkle.spray.SprayInputProcessor
import scala.reflect.runtime.{universe => ru}
import org.apache.hadoop.io.Writable

class SequenceInSpray[K<: Writable,V <: Writable](context: SparkContext, sequenceInputConfig: SequenceInputConfig, keyWritable:Class[K], ValueWritable:Class[V]) extends RDDInSpray[(K, V)](context, sequenceInputConfig) {

  def inSpray():RDD[(K, V)] =   {
    SprayInputProcessor.readSequenceFile(context,keyWritable, ValueWritable, sequenceInputConfig.path, sequenceInputConfig.partitons)
  }  
        
  
}
