package com.dataplatform.sparkle.util

object ReflectionUtil {

  implicit def string2Class[T <: AnyRef](name: String)(
      implicit classLoader: ClassLoader): Class[T] = {
    val clazz = Class.forName(name, true, classLoader)
    clazz.asInstanceOf[Class[T]]
  }
}
