package com.dataplatform.sparkle.spray.parquet

import org.apache.spark.sql.DataFrame
import com.dataplatform.sparkle.spray.SprayInputProcessor
import com.dataplatform.sparkle.spray.dataframe.DataFrameInSpray
import org.apache.spark.sql.SQLContext
import com.dataplatform.sparkle.spray.csv.CSVInputConfig

class ParquetInSpray(context: SQLContext, parquetInputConfig: ParquetInputConfig) extends DataFrameInSpray(context, parquetInputConfig) {

  def inSpray(): DataFrame = {
    SprayInputProcessor.readParquet(context, parquetInputConfig.path)
  }

}