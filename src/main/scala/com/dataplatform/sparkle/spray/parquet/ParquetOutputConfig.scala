package com.dataplatform.sparkle.spray.parquet

import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
 * @author sarajagopal
 */
case class ParquetOutputConfig(sprayName: String, directoryPath: String,  rePartition: Int,  saveMode: String) extends SparkleOutputConfig