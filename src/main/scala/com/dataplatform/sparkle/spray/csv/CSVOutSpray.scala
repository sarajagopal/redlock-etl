package com.dataplatform.sparkle.spray.csv

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.DataFrame
import com.dataplatform.sparkle.spray.OutSpray
import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import com.dataplatform.sparkle.spray.dataframe.DataFrameOutSpray

class CSVOutSpray(csvOutputConfig: CSVOutputConfig)
    extends DataFrameOutSpray(csvOutputConfig) {

  def outSpray(df: DataFrame) {
    SparkleOutputProcessor.writeCSVFile(df,
                                        csvOutputConfig.format,
                                        csvOutputConfig.getOptions(),
                                        csvOutputConfig.saveMode,
                                        csvOutputConfig.filePath,
                                        csvOutputConfig.rePartition)
  }

}
