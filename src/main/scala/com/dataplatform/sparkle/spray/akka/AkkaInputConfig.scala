package com.dataplatform.sparkle.spray.akka

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
  * @author sarajagopal
  */
case class AkkaInputConfig(sprayName: String,
                           actorName: String,
                           storageLevel: String)
    extends SparkleInputConfig
