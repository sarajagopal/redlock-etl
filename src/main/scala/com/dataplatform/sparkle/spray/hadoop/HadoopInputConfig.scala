package com.dataplatform.sparkle.spray.hadoop

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
 * @author sarajagopal
 */
case class HadoopInputConfig( sprayName: String,  path: String,  hadoopKeyType: String,  hadoopValueType: String,  inputFormat: String)  extends SparkleInputConfig