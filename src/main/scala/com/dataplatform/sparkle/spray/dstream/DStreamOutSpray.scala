package com.dataplatform.sparkle.spray.dstream

import org.apache.spark.streaming.dstream.DStream
import com.dataplatform.sparkle.config.SparkleOutputConfig
import com.dataplatform.sparkle.spray.OutSpray

abstract class DStreamOutSpray[T](sparkleOutputConfig: SparkleOutputConfig) extends OutSpray[DStream[T]](sparkleOutputConfig) with Serializable {
  
  def preSpray(){    
  }
  
  
  def outSpray(dStream: DStream[T])
  
   
  def postSpray(){    
  }

}