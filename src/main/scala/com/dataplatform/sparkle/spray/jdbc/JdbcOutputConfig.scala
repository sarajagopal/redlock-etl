package com.dataplatform.sparkle.spray.jdbc

import com.dataplatform.sparkle.config.SparkleOutputConfig

/**
  * @author sarajagopal
  */
case class JdbcOutputConfig(sprayName: String,
                            url: String,
                            driver: String,
                            table: String,
                            userName: String,
                            password: String,
                            saveMode: String)
    extends SparkleOutputConfig
