package com.dataplatform.sparkle.spray.orc

import com.dataplatform.sparkle.spray.dataframe.DataFrameOutSpray
import com.dataplatform.sparkle.spray.SparkleOutputProcessor
import org.apache.spark.sql.DataFrame

class ORCOutSpray (orcOutputConfig: ORCOutputConfig) extends DataFrameOutSpray(orcOutputConfig) {

  def outSpray(df: DataFrame) {
    SparkleOutputProcessor. writeORCFile(df,orcOutputConfig.directoryPath,orcOutputConfig.saveMode,orcOutputConfig.rePartition)
}
  
}