package com.dataplatform.sparkle.config

/**
  * @author sarajagopal
  */
case class SparkContextConfig(
    createSparkSqlContext: Boolean = false,
    createHiveContext: Boolean = false,
    checkPointDirectory: Option[String] = None,
    sparkStreamingContextConfig: Option[SparkStreamingContextConfig] = None)
