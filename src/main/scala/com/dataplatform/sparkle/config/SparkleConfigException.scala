package com.dataplatform.sparkle.config

class SparkleConfigException(message: String = null, cause: Throwable = null)
    extends RuntimeException(message, cause)
