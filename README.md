# README #

###Install
* Gradle 5.2.1 - 
   In the terminal ( use sdkman instead of brew)
   curl -s "https://get.sdkman.io" | bash
   source "/Users/sarajagopal/.sdkman/bin/sdkman-init.sh"
   sdk install gradle 5.2.1
   sdk default gradle 5.2.1
   export GRADLE_HOME="/Users/sarajagopal/.sdkman/candidates/gradle/5.2.1" 
   export PATH=$PATH:$GRADLE_HOME/lib
* Scala 2.11.12 :
   In the terminal - brew install scala@2.11
   echo 'export PATH="/usr/local/opt/scala@2.11/bin:$PATH"' >> ~/.bash_profile
   source ~/bash_profile
* Java 1.8
* add plugin Scala and Gradle

### What is this repository for? ###

* Quick summary

   The objective for the framework - 
   
      1. S3 Functionality ( added functionality on top of AmazonS3Client )
      2. Spray ( text-streaming / Akka-streams )
      3. Convert yml configuration files to case classes.
      4. Spark Context Helper

* Version - 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact