package com.dataplatform.sparkle.spray.parquet

import com.dataplatform.sparkle.config.SparkleInputConfig

/**
 * @author sarajagopal
 */
case class ParquetInputConfig(sprayName: String,path: String) extends SparkleInputConfig